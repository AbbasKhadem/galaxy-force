﻿public enum sysLang
{
    //Afrikaans = 0,
    //
    // Summary:
    //     Arabic.
    Arabic = 1,
    //
    // Summary:
    //     Basque.
    /*Basque = 2,
    //
    // Summary:
    //     Belarusian.
    Belarusian = 3,
    //
    // Summary:
    //     Bulgarian.
    Bulgarian = 4,
    //
    // Summary:
    //     Catalan.
    Catalan = 5,
    //
    // Summary:
    //     Chinese.
    Chinese = 6,
    //
    // Summary:
    //     Czech.
    Czech = 7,
    //
    // Summary:
    //     Danish.
    Danish = 8,
    //
    // Summary:
    //     Dutch.
    Dutch = 9,
    //
    // Summary:
    //     English.*/
    English = 10,
    //
    // Summary:
    //     Estonian.
    //Estonian = 11,
    //
    // Summary:
    //     Faroese.
    //Faroese = 12,
    //
    // Summary:
    //     Finnish.
   // Finnish = 13,
    //
    // Summary:
    //     French.
    French = 14,
    //
    // Summary:
    //     German.
    German = 15,
    //
    // Summary:
    //     Greek.
    /*Greek = 16,
    //
    // Summary:
    //     Hebrew.
    Hebrew = 17,
    //
    // Summary:
    //     Hungarian.
    Hugarian = 18,
    //
    // Summary:
    //     Icelandic.
    Icelandic = 19,
    //
    // Summary:
    //     Indonesian.
    Indonesian = 20,
    //
    // Summary:
    //     Italian.*/
    Italian = 21,
    //
    // Summary:
    //     Japanese.
   /* Japanese = 22,
    //
    // Summary:
    //     Korean.
    Korean = 23,
    //
    // Summary:
    //     Latvian.
    Latvian = 24,
    //
    // Summary:
    //     Lithuanian.
    Lithuanian = 25,
    //
    // Summary:
    //     Norwegian.
    Norwegian = 26,
    //
    // Summary:
    //     Polish.
    Polish = 27,
    //
    // Summary:
    //     Portuguese.
    Portuguese = 28,
    //
    // Summary:
    //     Romanian.
    Romanian = 29,
    //
    // Summary:
    //     Russian.
    Russian = 30,
    //
    // Summary:
    //     Serbo-Croatian.
    SerboCroatian = 31,
    //
    // Summary:
    //     Slovak.
    Slovak = 32,
    //
    // Summary:
    //     Slovenian.
    Slovenian = 33,
    //
    // Summary:
    //     Spanish.
    Spanish = 34,
    //
    // Summary:
    //     Swedish.
    Swedish = 35,
    //
    // Summary:
    //     Thai.
    Thai = 36,
    //
    // Summary:
    //     Turkish.
    Turkish = 37,
    //
    // Summary:
    //     Ukrainian.
    Ukrainian = 38,
    //
    // Summary:
    //     Vietnamese.
    Vietnamese = 39,
    //
    // Summary:
    //     ChineseSimplified.
    ChineseSimplified = 40,
    //
    // Summary:
    //     ChineseTraditional.
    ChineseTraditional = 41,
    //
    // Summary:*/
    //     Unknown.
    Persian = 42,

}